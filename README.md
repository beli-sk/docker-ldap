LDAP docker image
=================

Docker image of OpenLDAP server.


Locations
---------

Source of the image is hosted on Bitbucket at
https://bitbucket.org/beli-sk/docker-ldap

If you find any problems, please post an issue at
https://bitbucket.org/beli-sk/docker-ldap/issues

The built image is located on Docker Hub at
https://hub.docker.com/r/beli/ldap/


Pull or build
-------------

The image is built automatically on Docker hub in repository **beli/ldap**
and can be pulled using command

    docker pull beli/ldap

or if you'd prefer to build it yourself from the source repository

    git clone https://bitbucket.org/beli-sk/docker-ldap.git
    cd docker-ldap/
    docker build -t beli/ldap ldap/


Usage
-----

The container requires two volumes: `/config` for the dynamic configuration
database and `/data` for the directory database. The volumes should be owned
and writable by UID 104, GID 107. If not, the container will attempt to change
the ownership to these values and refuses to start if it fails.

If you supply empty configuration volume, it will be initialized with default
values. In this case you need to supply environment variables `CONF_ROOTPW`
for the admin password, either plain text or hashed in an OpenLDAP supported
format and `CONF_BASEDN` for the base of the tree to be served. The default
admin account has RDN `cn=admin` at the base DN you supply and DN
`cn=admin,cn=config` with access to config tree, both with the ROOTPW you
supply.

Please note that if you supply plaintext password, it cannot contain a leading
curly bracket `{` or it will be incorrectly recognized as hashed. A plaintext
password will be hashed before saving.

Database initialization can be done from LDIF files placed in
`/initdb.d/*.ldif`. They will be processed with `ldapadd` in alphabetical
order.

When you supply an existing configuration, these parameters are ignored and
the configuration is used as is.

The container exposes TCP port 389 for LDAP protocol.

So for example, to start with `config` and `data` volumes mounted from current
directory on the host:

    docker run --name ldap --rm -v `pwd`/config:/config -v `pwd`/data:/data \
      -p 389:389 -e CONF_ROOTPW=changeme -e CONF_BASEDN=dc=example,dc=com beli/ldap

Note that `CONF_ROOTPW` and `CONF_BASEDN` parameters are required only on first run
and can be omited after successful initialization of config and data directories.
